from dateutil import *
from dateutil.rrule import *
from dateutil.parser import *
from datetime import *
import itertools

file = open("emsoji.txt","w")

def generate_emso(dat_roj,register,zaporedna):
    date = '{dt:%d}{dt:%m}{year}'.format(dt=dat_roj, year='{dt:%Y}'.format(dt=dat_roj)[1:])
    
    out = date+str(register)+str(zaporedna).zfill(3)
    emso = emso_verify(out)
    if emso:
        file.write(emso+"\n")

#https://sl.wikipedia.org/wiki/Enotna_matična_številka_občana#Izra.C4.8Dun_v_programskem_jeziku_Python    
def emso_verify(emso):
        """
        Accepts a number of 12-13 digits and returns the number
        with a valid 13th control digit, details (in slovene) at
        http://www.uradni-list.si/1/objava.jsp?urlid=19998&stevilka=345
        """
        emso_factor_map = [7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2]
        def emso_digit(emso, place): # returns int typed emso digit
                try:
                        return int(emso[place])
                except IndexError:
                        return 0
        emso_sum = 0
        for digit in range(12):
                emso_sum += emso_digit(emso, digit) * emso_factor_map[digit]
        ostanek = emso_sum % 11
        
        if ostanek == 0:
            control_digit = 0
        elif ostanek == 1:
            return None
        else:
            control_digit = 11 - ostanek
            
        return str(emso)[:12] + str(control_digit)
    
dat_rojstva = rrule(DAILY, dtstart=datetime(1900,1,1),until=datetime(2050,1,1))

#http://www.mnz.gov.si/si/mnz_za_vas/osebni_in_tajni_podatki/emso/
#st_registrov = xrange(10,99,1)
st_registrov = [50] 

st_zapored = xrange(0,999,1)

for dat, reg, zapo in itertools.product(dat_rojstva, st_registrov, st_zapored):
    generate_emso(dat, reg, zapo);
    